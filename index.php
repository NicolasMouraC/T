<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Streams da Twitch</title>
    <style>
      body {
          font-family: Arial, sans-serif;
          margin: 0;
          padding: 0;
          background-color: #f7f7f7;
      }
      #twitch-streams {
          position: absolute;
          bottom: -40%;
          right: -35%;
          width: 50%;
          height: 100%;
          padding: 20px;
          box-sizing: border-box;
      }
      .stream-container {
          position: relative;
          width: 300px;
          margin-bottom: 10px;
          background-color: #fff;
          border-radius: 8px;
          overflow: hidden;
          box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
          transition: transform 0.2s;
      }
      .stream-container:hover {
          transform: translateY(-5px);
      }
      .stream-container img {
          width: 100%;
          height: auto;
      }
      .stream-info {
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          padding: 10px;
          background-color: rgba(247, 247, 247, 0.8);
      }
      .stream-info p {
          margin: 0;
          font-size: 14px;
          color: #666;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
      }
      .stream-info p.username {
          font-weight: bold;
          color: #333;
      }
      .stream-info p.viewers {
          color: #4caf50;
      }
      .live-tag {
          position: absolute;
          top: 10px;
          right: 10px;
          background-color: #4caf50;
          color: #fff;
          padding: 5px 10px;
          border-radius: 5px;
          font-size: 12px;
          text-transform: uppercase;
      }

      .main-container {
          height: 400px;
          width: 70%;
          background-color: #4caf50;
          position: relative;
      }

      .streamer {
          display: flex;
          align-items: center;
          margin-top: 10px;
      }
      .streamer img {
          width: 45px;
          height: 45px;
          border-radius: 50%;
          margin-right: 10px;
          float: left; /* Altere para 'right' se desejar */
      }
      .streamer .info {
          display: flex;
          flex-direction: column;
      }
      .streamer .info h3 {
          margin: 0;
          font-size: 14px;
          font-weight: bold;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
      }
      .streamer .info p {
          margin: 0;
          font-size: 12px;
          color: #666;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
      }
  </style>
</head>
<body>
<div style="display: flex; justify-content: center;">
  <div class="main-container">
    <div id="twitch-streams"></div>
    <div id="streamers-list"></div>
  </div>
</div>

<?php
$clientId = '67xk9fcw9fonxd9g4ioq4f5s7dvfrf';
$clientSecret = '93zs7x9ipq10fqo9d1973zwekjnacu';
$accessTokenUrl = 'https://id.twitch.tv/oauth2/token';
$streamersName = ["fearzuu", "guerreirotetra", "jardineiroxd", "maikezerasz", "jorgindadozeeee"];
$params = http_build_query([
    'client_id' => $clientId,
    'client_secret' => $clientSecret,
    'grant_type' => 'client_credentials'
]);
$accessToken = null;

function getAccessToken() {
    global $accessToken, $accessTokenUrl, $params;
    try {
        $response = file_get_contents("$accessTokenUrl?$params", false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
            ]
        ]));
        $data = json_decode($response, true);
        $accessToken = $data['access_token'];
    } catch (Exception $e) {
        echo 'Erro ao obter access token: ',  $e->getMessage(), "\n";
    }
}

function fetchAndDisplayStreamers() {
    global $accessToken, $clientId, $streamersName;
    getAccessToken();
    $formattedStreamersName = implode('&user_login=', $streamersName);
    $apiEndpoint = "https://api.twitch.tv/helix/streams?user_login=$formattedStreamersName&type=live&first=5";

    try {
        $context = stream_context_create([
            'http' => [
                'header' => "Authorization: Bearer $accessToken\r\n" .
                            "Client-ID: $clientId\r\n"
            ]
        ]);
        $response = file_get_contents($apiEndpoint, false, $context);
        $data = json_decode($response, true);
        $twitchStreamsElement = '<div id="twitch-streams">';
        
        foreach ($data['data'] as $stream) {
            $streamElement = '<div class="stream-container">';
            $streamElement .= '<a href="https://www.twitch.tv/'.$stream['user_name'].'" target="_blank">';
            $streamElement .= '<img src="'.str_replace('{width}', '300', str_replace('{height}', '200', $stream['thumbnail_url'])).'">';
            $streamElement .= '</a>';
            $streamElement .= '<div class="stream-info">';
            $streamElement .= '<p class="username">'.$stream['title'].'</p>';
            $streamElement .= '<div class="streamer">';
            $streamElement .= '<img id="'.$stream['user_name'].'" src="">';
            $streamElement .= '<div class="info">';
            $streamElement .= '<h3>'.$stream['user_name'].'</h3>';
            $streamElement .= '<p>'.$stream['viewer_count'].' viewers</p>';
            $streamElement .= '</div></div></div><div class="live-tag">Ao Vivo</div></div>';
            $twitchStreamsElement .= $streamElement;
        }
        $twitchStreamsElement .= '</div>';
        echo $twitchStreamsElement;
    } catch (Exception $e) {
        echo 'Erro ao buscar streams: ',  $e->getMessage(), "\n";
    }
}

function fetchAndDisplayStreamersInfo() {
    global $accessToken, $clientId, $streamersName;
    getAccessToken();
    $apiUrl = "https://api.twitch.tv/helix/streams?user_login=".implode('&user_login=', $streamersName);

    try {
        $context = stream_context_create([
            'http' => [
                'header' => "Authorization: Bearer $accessToken\r\n" .
                            "Client-ID: $clientId\r\n"
            ]
        ]);
        $response = file_get_contents($apiUrl, false, $context);
        $data = json_decode($response, true);
        $streamersListElement = '<div id="streamers-list">';
        
        foreach ($data['data'] as $stream) {
            $userResponse = file_get_contents("https://api.twitch.tv/helix/users?login=".$stream['user_name'], false, $context);
            $userData = json_decode($userResponse, true);
            $user = $userData['data'][0];

            $streamerElement = '<div class="streamer">';
            $streamerElement .= '<img id="'.$stream['user_name'].'" src="'.$user['profile_image_url'].'" alt="Foto de perfil de '.$stream['user_name'].'">';
            $streamerElement .= '<div class="info">';
            $streamerElement .= '<h3>'.$stream['user_name'].'</h3>';
            $streamerElement .= '<p>'.$stream['viewer_count'].' viewers</p>';
            $streamerElement .= '</div></div>';
            $streamersListElement .= $streamerElement;
        }
        $streamersListElement .= '</div>';
        echo $streamersListElement;
    } catch (Exception $e) {
        echo 'Erro ao buscar streamers: ',  $e->getMessage(), "\n";
    }
}


fetchAndDisplayStreamers();
fetchAndDisplayStreamersInfo();
?>
</body>
</html>
